# asset-kyc-converter

## Context

Codefi Asset allow our client to configure the onboarding form by specifying the needed fields in an Excel file

More document on Codefi Configuration can be found in [Confluence](https://consensysteam.atlassian.net/wiki/spaces/PS/pages/667976021/Codefi+Assets)

Based on the Excel file filled in by the client we create two Json files which will be used to configure the Codefi Asset KYC-api ???? and the Front-end.

More details on the business rules can be found below

### Goal

The goal is to convert the excel file in an automated way to shorten the delivery of Codefi Asset configuration projects and ensure the output meets the requirements before sharing with the Codefi Asset Team

## Implemented Business rules

The converter implements business rules documented [in Confluence](https://consensysteam.atlassian.net/wiki/spaces/PS/pages/764117032/KYC+templates+and+elements)

### Available steps

1. create the 2 json files
2. create the KYC element
3. remove the KYC elements from this tenant
4. create the new KYC template and update the KYC templateId for the Issuer
5. update the KYC template

The next steps could be to extend this project to allow


## How to use

The tool should be used to convert an .xlsx file compliant with this [template](https://docs.google.com/spreadsheets/d/1yMy1AAUdmFiJlSPLFy1hia981DRFsISiemfecQRS5XY/view)

### Prerequisite

You need Node.js to run it

You can install it from the [Node.js website](https://nodejs.org/en/download/)

### How to use

1. Clone the repository

```sh
git clone git@gitlab.com:ConsenSys/ .git
```

2. Install dependencies

```sh
cd asset-kyc-converter
```

```sh
npm install
```

3. Configure the environment

```sh
cp .env.sample .env.<client>.dev
cp .env.sample .env.<client>.demo
```

Update the configuration files for dev and demo environments.

4. Convert the excel KYC configuration

```sh
node bin/kyc -t kyc-convert -e .env.<client>.<dev|demo>
```

Your files are created in the folder.

4. Delete KYC elements (if needed)

```sh
node bin/kyc -t kyc-elements-delete -e .env.<client>.<dev|demo>
```


5. Create KYC elements


```sh
node bin/kyc -t kyc-elements-post -e .env.<client>.<dev|demo>
```


6. Create KYC template (if needed)


```sh
node bin/kyc -t kyc-elements-post -e .env.<client>.<dev|demo>
```


6. Update KYC template (if needed)


```sh
node bin/kyc -t kyc-template-put -e .env.<client>.<dev|demo>
```

```
