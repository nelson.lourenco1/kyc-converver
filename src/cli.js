import chalk from 'chalk';
import { createKycFiles } from './create-files/create-files';
import { deleteElements } from './delete/delete-elements';
import { postElementsBatch } from './post/post-elements';
import { postTemplate } from './post/post-template';
import { putTemplate } from './put/put-template';

export async function cli(options) {
  switch (options.task) {
    case 'kyc-convert': {
      console.log(
        `\n\nGenerating JSON file(s) for Excel input %s\n\n`,
        chalk.magentaBright.bold(options.FILE_PATH),
      );
      await createKycFiles(options);
      break;
    }
    case 'kyc-elements-delete': {
      console.log(`\n\nDelete KYC elements \n\n`);
      await deleteElements(options);
      break;
    }
    case 'kyc-elements-post': {
      console.log(`\n\nCreate KYC elements\n\n`);
      await postElementsBatch(options);
      break;
    }
    case 'kyc-template-post': {
      console.log(`\n\nCreate KYC template\n\n`);
      await postTemplate(options);
      break;
    }
    case 'kyc-template-put': {
      console.log(`\n\nUpdate KYC template\n\n`);
      await putTemplate(options);
      break;
    }
    case 'asset': {
      console.error('\n\nError: Sorry, Not implemented yet');
      break;
    }
  }
}
