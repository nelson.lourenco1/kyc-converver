import chalk from 'chalk';
import fs from 'fs';
import xlsxFile from 'read-excel-file/node';

import i18nIsoCountries from 'i18n-iso-countries';
const countryData = require('country-telephone-data').allCountries;

const confs = {
  // NewTemplateOrg sheet
  ConfigurationSheet: 'NewTemplateOrg',
  ConfigurationSheetInd: 0,
  // key columns
  TopSectionKey: 0,
  SectionKey: 1,
  ElementKey: 2,
  ElementType: 3,
  ElementStatus: 4,
  ElementData: 5,
  ElementOptions: 6,
  // Lang EN
  EnLangLabelsStartColumn: 7,
  TopSectionLabel: 8,
  SectionLabel: 9,
  SectionDesc: 10,
  ElementLabel: 11,
  ElementPlaceHolder: 12,
  ElementOptionsLabels: 13,

  // column where starts a new language
  StartAdditionalLangColumn: 14,

  // columns - lang + number of labels
  LangLabels: 7,

  // InputRelatedItems sheet
  InputRelatedItems: 'InputRelatedItems',
  InputRelatedItemsInd: 1,
  //columns
  ElementWithRelationships: 0,
  RelatedItems: 1,
  RelatedInput: 2,

  AllowedTopSections: ['naturalPersonSection', 'legalPersonSection'],
  MultiOptionTypes: ['check', 'radio', 'multistring'],
  AllowedTypes: [
    'string',
    'number',
    'check',
    'radio',
    'document',
    'multistring',
    'date',
    'title',
    'codefi',
  ],
};

const template = {
  issuerId: 'Professional Services',
  name: 'Codefi KYC template',
  data: {},
  topSections: [],
};
const elements = [];
let validationErrorCount = 0;
const countries = [];
const countryCodes = [];

export async function createKycFiles(options) {
  const filePath = options.FILE_PATH;
  const elementsFile = options.TARGET_FOLDER + 'elements.json';
  const templateFile = options.TARGET_FOLDER + 'template.json';

  // Add / Update custom name for template
  if (options.clientName) {
    template.issuerId = `${options.clientName} ${template.issuerId}`;
    template.name = template.name.replace('Codefi', options.CLIENT_NAME);
  }

  let sheets;
  try {
    sheets = await Promise.all([
      xlsxFile(filePath, {
        sheet: confs.ConfigurationSheet,
      }),
      xlsxFile(filePath, {
        sheet: confs.InputRelatedItems,
      }),
    ]);

    const rawRelatedItems = sheets[confs.InputRelatedItemsInd];

    // Cleanup- Remove all new line(\n) and carriage return(\r) from related items
    rawRelatedItems.forEach((rawRelatedItem) => {
      rawRelatedItem[confs.RelatedItems] = rawRelatedItem[confs.RelatedItems]
        .split(',')
        .map((item) => item.replace(/^\s+|\n/g, ''));
    });

    let rows = sheets[confs.ConfigurationSheetInd];

    // Remove first row/headers of sheet NewTemplateOrg
    rows.shift();

    //check the input file for validation
    checkFile(rows);
    validationError();

    // populate Country Names and Country Telephone Codes
    populateCountryData(getI18NLangs(rows));

    //Create the elements.json and add it to the array of elements
    for (let row of rows) {
      let element = createElement(row, rawRelatedItems);
      if (element != null) {
        elements.push(element);
      }
    }

    let template = addElementToTemplate(rows);
    const elementsJson = JSON.stringify(elements);
    fs.writeFileSync(elementsFile, elementsJson);
    console.log(
      `\n%s elements.json contains ${elements.length.toString()} elements\n`,
      chalk.magentaBright.bold('PLEASE CHECK THE ELEMENTS'),
    );

    const templateJson = JSON.stringify(template);
    fs.writeFileSync(templateFile, templateJson);
    console.log(
      `\n%s, Output Files Ready: ${templateFile}  &  ${elementsFile}`,
      chalk.green.bold('DONE'),
    );
  } catch (error) {
    console.error(error);
  }
}

function getI18NLangs(rows) {
  let langs = ['en'];
  let langColumn = confs.StartAdditionalLangColumn;

  const firstRow = rows[0];

  while (langColumn < firstRow.length) {
    langs.push(firstRow[langColumn]);
    langColumn += confs.LangLabels;
  }

  return langs;
}

function getI18NLabelsIndex(row, index) {
  const valueEn = row[confs.ElementOptionsLabels]
    .split(', ')
    .map((item) => item.trim())[index];

  const labels = {};

  let langColumn = confs.EnLangLabelsStartColumn;
  let labelColumn = confs.ElementOptionsLabels;

  while (labelColumn < row.length) {
    const value = row[labelColumn].split(', ').map((item) => item.trim())[
      index
    ];
    if (valueEn !== undefined && value === undefined) {
      console.warn(
        `- missing translated option for ${chalk.redBright.bold(
          valueEn,
        )} - kyc element ${chalk.redBright.bold(
          row[confs.ElementKey],
        )} - lang ${chalk.redBright.bold(row[langColumn])}`,
      );
    }

    labels[row[langColumn]] = value;

    langColumn += confs.LangLabels;
    labelColumn += confs.LangLabels;
  }

  return labels;
}

function getI18NLabels(row, enLabelPosition) {
  if (row[enLabelPosition] === null) {
    return;
  }

  const labels = {
    en: row[enLabelPosition],
  };

  let langColumn = confs.StartAdditionalLangColumn;
  let labelColumn = confs.LangLabels + enLabelPosition;

  while (labelColumn < row.length) {
    labels[row[langColumn]] = row[labelColumn];

    langColumn += confs.LangLabels;
    labelColumn += confs.LangLabels;
  }

  return labels;
}

/**
 * createElement take a row and the related Items and returns an element
 * @param {string[], string[][]}
 * @return {element}
 */
function createElement(row, sheetRelatedItems) {
  const type = row[confs.ElementType].toLowerCase();

  // uses the codefi kyc element
  if (type == 'codefi') {
    return;
  }

  let element = {
    key: row[confs.ElementKey],
    type: type,
    status: row[confs.ElementStatus],
    data: {},
    label: {},
    placeholder: null,
  };

  element.label = getI18NLabels(row, confs.ElementLabel);
  element.placeholder = getI18NLabels(row, confs.ElementPlaceHolder);

  //element of type "check", "radio" and "multistring" should have inputs
  if (confs.MultiOptionTypes.includes(element.type)) {
    element.inputs = [];
    const elementOptions = row[confs.ElementOptions];
    //check that there are inputs
    if (elementOptions !== null) {
      switch (elementOptions) {
        case 'Countries': {
          element.inputs = countries;
          break;
        }
        case 'Country Codes': {
          element.inputs = countryCodes;
          break;
        }
        default: {
          //input can have "relatedElements"
          let inputs = elementOptions.split(', ').map((item) => item.trim());

          inputs.forEach((option, i) => {
            let inputToAdd = {
              value: option,
              label: {},
            };
            inputToAdd.label = getI18NLabelsIndex(row, i);

            let relatedItems = addRelatedItems(
              option,
              element.key,
              sheetRelatedItems,
            );

            inputToAdd.relatedElements = relatedItems;
            element.inputs.push(inputToAdd);
          });
        }
      }
    }
  }

  // 'data' column is required for all elements of 'conditional' status
  let elementStatus = row[confs.ElementStatus];

  if (elementStatus === 'conditional') {
    let elementData = row[confs.ElementData];

    if (
      elementData !== null &&
      (elementData === 'mandatory' || elementData === 'optional')
    ) {
      element.data = { validation: { status: elementData } };
    } else {
      console.log(
        `${++validationErrorCount}. Error: column 'Data For Conditional Elements in sheet: KycForm (Please fill this one i)' is required with value either 'mandatory/optional' for element %s of %s status`,
        chalk.redBright.bold(`${row[confs.SectionKey]}`),
        chalk.redBright.bold(`${elementStatus}`),
      );
    }
  }

  return element;
}

function addRelatedItems(i, elementKey, r) {
  const e = r.find(
    (item) =>
      i === item[confs.RelatedInput] &&
      elementKey === item[confs.ElementWithRelationships],
  );
  if (e) {
    return e[confs.RelatedItems];
  }
}

/**
 * addElementToTemplate
 * @return {string[]}
 * @param rows
 */
function addElementToTemplate(rows) {
  rows.forEach((row) => {
    let topSection = addTopSection(row);

    if (topSection.key === row[confs.TopSectionKey]) {
      let section = addSection(row, topSection);

      if (section.key === row[confs.SectionKey]) {
        let element = row[confs.ElementKey];
        section.elements.push(element);
      }
    }
  });

  return template;
}

function addTopSection(row) {
  let topSection = {
    key: row[confs.TopSectionKey],
    label: {},
    sections: [],
  };

  let tsFilter = template.topSections.filter((ts) => ts.key === topSection.key);
  if (tsFilter.length === 0) {
    topSection.label = getI18NLabels(row, confs.TopSectionLabel);

    template.topSections.push(topSection);
  } else {
    topSection = tsFilter[0]; // the only existing top section with TopSectionKey
  }

  return topSection;
}

function addSection(row, topSection) {
  let section = {
    key: row[confs.SectionKey],
    label: {},
    description: {},
    elements: [],
  };

  const sectionFilter = topSection.sections.filter(
    (s) => s.key === section.key,
  );
  if (sectionFilter.length === 0) {
    section.label = getI18NLabels(row, confs.SectionLabel);
    section.description = getI18NLabels(row, confs.SectionDesc);
    topSection.sections.push(section);
  } else {
    section = sectionFilter[0]; // the only existing section with SectionKey
  }

  return section;
}

function checkFile(rows) {
  let currentRow = 1;
  rows.forEach((row) => {
    currentRow++;

    //Check top section keys
    if (!confs.AllowedTopSections.includes(row[confs.TopSectionKey])) {
      console.log(
        `${++validationErrorCount}. Error: The top section of %s for element %s is invalid - Row %s of sheet 'NewTemplateOrg'`,
        chalk.redBright.bold(`${row[confs.TopSectionKey]}`),
        chalk.redBright.bold(`${row[confs.ElementKey]}`),
        chalk.redBright.bold(`${currentRow}`),
      );
    }

    //Check types
    let elementType = row[confs.ElementType];
    if (
      !elementType ||
      !confs.AllowedTypes.includes(elementType.toLowerCase())
    ) {
      console.log(
        `${++validationErrorCount}. Error: The elementType of %s for element %s is invalid - Row %s of sheet 'NewTemplateOrg'`,
        chalk.redBright.bold(`${elementType}`),
        chalk.redBright.bold(`${row[confs.ElementKey]}`),
        chalk.redBright.bold(`${currentRow}`),
      );
    } else {
      elementType = elementType.toLowerCase();
    }

    //Check inputs for specific element types
    if (confs.MultiOptionTypes.includes(elementType)) {
      let elementOptions = row[confs.ElementOptions];
      if (elementOptions) {
        const inputs = elementOptions.split(', ').map((item) => item.trim());
        switch (elementType) {
          case 'check':
          case 'multistring': {
            if (inputs.length < 1) {
              console.log(
                `${++validationErrorCount}. Error: 'Input %s for elementType of %s for element %s should have at least one input - Row %s of sheet 'NewTemplateOrg'`,
                chalk.redBright.bold(`${elementOptions}`),
                chalk.redBright.bold(`${row[confs.ElementKey]}`),
                chalk.redBright.bold(`${row[confs.ElementType]}`),
                chalk.redBright.bold(`${currentRow}`),
              );
            }
            break;
          }
          case 'radio': {
            if (inputs.length < 2) {
              console.log(
                `${++validationErrorCount}. Error: 'Input %s for elementType of %s for element %s should have at least two inputs - Row %s of sheet 'NewTemplateOrg'`,
                chalk.redBright.bold(`${elementOptions}`),
                chalk.redBright.bold(`${row[confs.ElementKey]}`),
                chalk.redBright.bold(`${row[confs.ElementType]}`),
                chalk.redBright.bold(`${currentRow}`),
              );
            }
            break;
          }
          default:
            break;
        }
      } else {
        console.log(
          `${++validationErrorCount}. Error: 'Input %s for elementType of %s for element %s is invalid - Row %s of sheet 'NewTemplateOrg'`,
          chalk.redBright.bold(`${elementOptions}`),
          chalk.redBright.bold(`${row[confs.ElementKey]}`),
          chalk.redBright.bold(`${row[confs.ElementType]}`),
          chalk.redBright.bold(`${currentRow}`),
        );
      }
    }
  });
}

function populateCountryData(langs) {
  const countryCodesUnsorted = [];

  countryData.forEach((country) => {
    countryCodesUnsorted.push(country.dialCode);
  });

  const countryCodesSortedAndUnique = Array.from(
    new Set(countryCodesUnsorted.sort()),
  );

  countryCodesSortedAndUnique.forEach((countryCode) => {
    const item = {
      label: {},
    };
    langs.map((lang) => {
      item.label[lang] = `+${countryCode}`;
    });

    countryCodes.push(item);
  });

  const countryNames = [];
  Object.entries(i18nIsoCountries.getAlpha2Codes()).forEach(([key, value]) => {
    countryNames.push(i18nIsoCountries.getName(key, 'en'));
  });

  const countryNamesSortedAndUnique = Array.from(new Set(countryNames.sort()));
  countryNamesSortedAndUnique.forEach((name) => {
    const iso2 = i18nIsoCountries.getAlpha2Code(name, 'en');
    const item = {
      value: name,
      label: {},
    };

    langs.forEach((lang) => {
      item.label[lang] = i18nIsoCountries.getName(iso2, lang);
    });
    countries.push(item);
  });
}

function validationError() {
  if (validationErrorCount > 0) {
    console.error(
      `\n\n %s Validation Errors found, Can not create JSON outputs, please fix invalid inputs and re-run the script to create JSON output file(s)`,
      chalk.redBright.bold(`${validationErrorCount}`),
    );

    throw new Error();
  }
}
