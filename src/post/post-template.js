import { codefiClientIssuer, oauthUser } from '../client/client';
import { putTemplateId } from '../put/put-user';

export async function postTemplate(options) {
  const template = require(`../../${options.TARGET_FOLDER}template.json`);

  const codefiAPI = await codefiClientIssuer(options);
  const user = await oauthUser(codefiAPI);

  console.log(`Posting template to assets-api`);

  template.issuerId = user.id;
  template.name = template.name + Date.now();

  try {
    const newTemplate = await codefiAPI.post(
      `/essentials/kyc/template?userType=ISSUER&userId=${user.id}`,
      template,
    );
    console.log(
      'KYC template successfully created - new KYC templateID:',
      newTemplate.data.template.id,
    );

    await putTemplateId(options, newTemplate.data.template.id);
  } catch (error) {
    console.log(`Template upload failed with message:${error.message}`);
    console.log(error.response);
  }
}
