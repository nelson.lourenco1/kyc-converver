import fs from 'fs';
import { codefiClientAdmin, oauthUser } from '../client/client';

/**
 *
 * @param {*} options
 * @returns
 */
export async function postElementsBatch(options) {
  const elements = require(`../../${options.TARGET_FOLDER}elements.json`);

  const codefiAPI = await codefiClientAdmin(options);
  const user = await oauthUser(codefiAPI);

  console.log(`Posting ${elements.length} elements to assets-api`);

  try {
    console.info(
      `create elements in batch - API URL ${options.CODEFI_API_ROOT}`,
    );

    await codefiAPI.post(`/essentials/kyc/element?userId=${user.id}`, elements);
  } catch (error) {
    console.log(error.message);

    postElements(options, codefiAPI, user);
    return;
  }

  console.log('Completed successfully');
}

/**
 *
 * @param {*} options
 */
export async function postElements(options, codefiAPI, user) {
  const elements = require(`../../${options.TARGET_FOLDER}elements.json`);

  let failedElements = [];

  console.log(`Posting ${elements.length} elements to assets-api`);
  console.warn('create KYC elements one by one');

  for (let i = 0; i < elements.length; i++) {
    const data = elements[i];
    try {
      console.info(`create element ${data.key} type: ${data.type}`);
      await codefiAPI.post(`/essentials/kyc/element?userId=${user.id}`, [data]);
    } catch (error) {
      console.error(error.message);
      let failedElement = elements[i];
      failedElement.error = error.message;
      failedElements.push(failedElement);
    }
  }

  if (failedElements.length == 0) {
    console.error(
      `${failedElements.length} failures out of ${elements.length}`,
    );

    const failedJSON = JSON.stringify(failedElements);

    fs.writeFile(
      `${options.TARGET_FOLDER}failed-elements-${Date.now()}.json`,
      failedJSON,
      function (err) {
        if (err) throw err;
        console.log('Saved failed elements failed-elements.json!');
      },
    );
  } else {
    console.log('Completed successfully');
  }
}
