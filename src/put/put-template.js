import { codefiClientIssuer, oauthUser } from '../client/client';

export async function putTemplate(options) {
  const template = require(`../../${options.TARGET_FOLDER}template.json`);

  const codefiAPI = await codefiClientIssuer(options);
  const user = await oauthUser(codefiAPI);

  console.log(`Posting template to assets-api`);

  const updateTemplate = {
    updatedParameters: {
      issuerId: user.id,
      name: template.name + Date.now(),
      topSections: template.topSections,
    },
  };

  template.issuerId = user.id;
  template.name = template.name + Date.now();

  console.log(user.data.kycTemplateId, template);

  try {
    await codefiAPI.put(
      `essentials/kyc/template/${user.data.kycTemplateId}?userId=${user.id}`,
      updateTemplate,
    );
    console.log('Completed successfully');
  } catch (error) {
    console.log(`Template upload failed with message:${error.message}`);
    console.log(error.response);
  }
}
