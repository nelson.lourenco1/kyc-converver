import { codefiClientIssuer, oauthUser } from '../client/client';

export async function putTemplateId(options, kycTemplateId) {
  const codefiAPI = await codefiClientIssuer(options);
  const user = await oauthUser(codefiAPI);

  console.log(`Update templateId for issuer to assets-api`);

  const updateUser = {
    updatedParameters: {
      data: {
        kycTemplateId,
      },
    },
  };
  console.log(updateUser);

  try {
    await codefiAPI.put(
      `essentials/user/${user.id}?userId=${user.id}`,
      updateUser,
    );
    console.log('Completed successfully - Update templateId');
  } catch (error) {
    console.log(`Template upload failed with message:${error.message}`);
    console.log(error.response);
  }
}
