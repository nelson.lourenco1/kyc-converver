import { codefiClientAdmin, oauthUser } from '../client/client';
import fs from 'fs';

export async function deleteElements(options) {
  const elements = require(`../../${options.TARGET_FOLDER}elements.json`);

  const codefiAPI = await codefiClientAdmin(options);

  const user = await oauthUser(codefiAPI);

  const kycData = await codefiAPI.get(
    `/essentials/kyc/element?userId=${user.id}`,
  );

  const kycElements = kycData.data.elements;

  const date = Date.now();
  fs.writeFileSync(
    `${options.TARGET_FOLDER}kyc-elements-from-api-${date}.json`,
    JSON.stringify(kycElements),
  );

  console.log(`Delete ${elements.length} KYC elements from assets-api`);

  elements.forEach((data) => {
    const results = kycElements.filter((e) => {
      return e.key === data.key && e.tenantId === user.tenantId;
    });

    results.forEach((e) => {
      console.log(`delete KYC element id: ${e.id}; key: ${e.key}`);
      codefiAPI.delete(`essentials/kyc/element/${e.id}?userId=${user.id}`);
      return;
    });
  });

  console.log('Completed successfully');
}
