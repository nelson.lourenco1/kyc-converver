import axios from 'axios';

export async function codefiClientAdmin(options) {
  return await codefiClient(options, options.AUTH_USERNAME_ADMIN);
}

export async function codefiClientIssuer(options) {
  return await codefiClient(options, options.AUTH_USERNAME_ISSUER);
}

export async function codefiClient(options, user) {
  
  
  const oauth = await axios.post(`${options.AUTH_URL}/oauth/token`, {
    grant_type: 'password',
    username: user,
    password: options.AUTH_PASSWORD,
    audience: options.AUTH_AUDIENCE,
    scope: 'openid profile email',
    client_id: options.AUTH_CLIENT_ID,
    client_secret: options.AUTH_CLIENT_SECRET,
  });

  const assetsURL = `${options.CODEFI_API_ROOT}/v2/`;

  const instance = axios.create({
    baseURL: assetsURL,
    headers: {
      Authorization: 'Bearer ' + oauth.data.access_token,
    },
  });

  return instance;
}

export async function oauthUser(instAxios) {
  const identity = await instAxios.get(`/utils/identity?userType=ISSUER`);

  return identity.data.user;
}
